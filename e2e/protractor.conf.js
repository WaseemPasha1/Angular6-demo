// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const {
  SpecReporter
} = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  directConnect: false,
  specs: [
    '../src/app/testing/testing.component.spec.ts'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {}
  },

  onPrepare: function () {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    var specs = [];
    var orgSpecFilter = jasmine.getEnv().specFilter;
    jasmine.getEnv().specFilter = function (spec) {
      specs.push(spec);
      return orgSpecFilter(spec);
    };
    jasmine.getEnv().addReporter(new function () {
      this.specDone = function (result) {
        if (result.failedExpectations.length > 0) {
          specs.forEach(function (spec) {
            spec.disable()
          });
        }
      };
    });
    var width = 1600;
    var height = 1200;
    browser.driver.manage().window().setSize(width, height);
  }
}
